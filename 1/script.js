let userWish = confirm('Хочешь сыграть в игру "Угадай число"?');
let countAttempts;
let score;
let randomNumber = getRandomInt(1, 10);
let btn = document.getElementById('btn');

function getWishUser(wish) {
  if (wish === true) {
    initGame();
  } else {
    document.getElementsByName("content")[0].style.display = 'none';
  }
}

function getRandomInt(min, max) {
  let randomInt = Math.floor(Math.random() * 10) + min;
  return randomInt;
}

function initGame() {
  countAttempts = 5;
  score = 0;

  document.getElementById("countAttempts").innerHTML = countAttempts;
  document.getElementById("score").innerHTML = score;
  document.getElementsByName("content")[0].style.display = 'block';
}

function getResultByClick() {
  let userNumber = document.getElementById("input").value;

  if (userNumber == randomNumber) {
    alert("Поздравляю! Вы угадали число:)");

    score += 10;
    document.getElementById("score").innerHTML = score;
    document.getElementById("countAttempts").innerHTML = countAttempts;

    randomNumber = getRandomInt(1, 10);
  } else {
    alert('К сожалению, вы не угадали. \nЗагадано новое число.');

    countAttempts--;
    document.getElementById("score").innerHTML = score;
    document.getElementById("countAttempts").innerHTML = countAttempts;

    randomNumber = getRandomInt(1, 10);

    if (countAttempts === 0) {
      gameOver();
    }
  }
}

function gameOver() {
  let userEndWish = confirm(`Game Over. У вас ${score} очков. Начать игру заново?`);
  getWishUser(userEndWish);
}



getWishUser(userWish);

btn.addEventListener('click', getResultByClick, false);
